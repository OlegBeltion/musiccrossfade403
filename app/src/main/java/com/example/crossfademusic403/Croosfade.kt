package com.example.crossfademusic403

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.animation.Animation


class Croosfade{
    var animator : ValueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f)

    var hsv : FloatArray = floatArrayOf(0.0f,1.0f,1.0f)
    var currentColor : Int? = null

    fun createAnimation(view: View, dur: Int){
        animator.apply {
            duration = dur.toLong() * 1000
            addUpdateListener {
                hsv[0] = 360 * animator.animatedFraction
                currentColor = Color.HSVToColor(hsv)
                view.setBackgroundColor(currentColor!!)
            }
            repeatCount = Animation.INFINITE
            start()
        }
    }

    fun stopAnimation(view: View){
        animator.cancel()
        view.setBackgroundColor(ContextCompat.getColor(view.context, R.color.lightPrimaryColor))
    }

}