package com.example.crossfademusic403

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.example.crossfademusic403.R.drawable.*

import java.lang.Exception

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener{

    private var seekBar : SeekBar? = null
    private var textViewSeekBar : TextView? = null
    private var btnFirstMp3 : Button? = null
    private var btnSecondMp3 : Button? = null
    private var btnPlay : Button? = null

    private var isPlayImg: Boolean = true // Флаг - определяет какая картинка на фоне кнопки проигрывания музыки
    private var imgPlay : Int = ic_play_arrow
    private var imgPause : Int = ic_pause_arrow

    private var flagUri : Int? = null // Флаг - определяет в какую переменную запишеся URI
    private var sounds : ArrayList<Uri> = ArrayList()
    private var firstSound: Uri? = null
    private var secondSound: Uri? = null
    private val READ_REQUEST_CODE = 1
    private var loopingPlay: LoopingPlay? = null

    private var rootView : View? = null
    private var croosfade : Croosfade? = null
    private var croosFadeTime : Int? =  2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        rootView = findViewById(R.id.linearLayoutMainActivity)
        initElem()
        setBtnClick()
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        Log.i("SeekBar","onProgressChange")
        val text = getString(R.string.tv_seekbar_crossfade) + progress
        textViewSeekBar?.text = text
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        Log.i("SeekBar","Touch")
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        Log.i("SeekBar","Stop Touch")
        if(seekBar!!.progress < 2){
            Toast.makeText(this,"Значение анимации не может быть меньше 2 секунд", Toast.LENGTH_SHORT).show()
            seekBar.progress = 2
        }
        croosFadeTime = seekBar.progress
    }

    private fun initElem() {

        seekBarInit()
        bntInit()

        loopingPlay = LoopingPlay()
        croosfade = Croosfade()
    }


    //Инициализация кнопок
    private fun bntInit() {
        btnFirstMp3 = findViewById(R.id.firstMp3)
        btnSecondMp3 = findViewById(R.id.secondMp3)
        btnPlay = findViewById(R.id.play)

        btnFirstMp3!!.text = getString(R.string.selectMp3)
        btnSecondMp3!!.text = getString(R.string.selectMp3)

        btnPlay!!.setBackgroundResource(imgPlay)
    }

    // Инициализация ползунка
    private fun seekBarInit() {
        Log.i("SeekBar","Init")

        seekBar = findViewById(R.id.seekBar)
        seekBar?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                min = 2
            } else{
                progress = 2
            }
            max = 10
            setOnSeekBarChangeListener(this@MainActivity)
        }

        val textSeekBar = getString(R.string.tv_seekbar_crossfade) + seekBar!!.progress
        textViewSeekBar = findViewById(R.id.tvCrossFade)
        textViewSeekBar?.apply {
            text = textSeekBar
        }
    }

    private fun setBtnClick() {

        btnFirstMp3!!.setOnClickListener{
            flagUri = 0
            getAudio()
        }
        btnSecondMp3!!.setOnClickListener{
            flagUri = 1
            getAudio()
        }
        btnPlay!!.setOnClickListener {
            if (isPlayImg){ // Проверка для смены кнопки проигрывания
                workWithAudio()
            }else{
                try {
                //play!!.pause()
                }catch (e: Exception){
                    e.printStackTrace()
                }
                croosfade!!.stopAnimation(rootView!!)
                btnPlay!!.setBackgroundResource(imgPlay)
                isPlayImg = true
            }
        }
    }
    // Crossfade
    private fun workWithAnimation() {
        try {
            croosfade!!.createAnimation(rootView!!, croosFadeTime!!)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun workWithAudio() {
        if (firstSound != null && secondSound != null){
            workWithAnimation()
            addAudio()
            loopingPlay!!.initData(sounds, applicationContext, croosFadeTime!!)
            getPlayerStatus()
            btnPlay!!.setBackgroundResource(imgPause)
            isPlayImg = false
        }else{
            Toast.makeText(this, "Добавьте оба аудиофайла", Toast.LENGTH_SHORT).show()
        }
    }

    // Выбор действий в зависимости от статуса плеера: Старт/Продолжить
    private fun getPlayerStatus() {
        if(loopingPlay!!.isPaused){
            loopingPlay!!.resume() // Продолжить
        }else{
            loopingPlay!!.play() // Старт
        }
    }

    // Добавление Uri песен в ArrayList
    private fun addAudio() {
        sounds.clear()
        sounds.add(firstSound!!)
        sounds.add(secondSound!!)
        Log.i("Sound 1","${sounds[0]}")
        Log.i("Sound 2","${sounds[1]}")
    }

    //Поиск аудио
    private fun getAudio() {
        val intent : Intent =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                Intent(Intent.ACTION_OPEN_DOCUMENT)
            }else{
                Intent(Intent.ACTION_GET_CONTENT)
            }

        intent.apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "audio/*"
        }
        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    // Получение Uri на аудиофайл
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if (data != null){
                val uri : Uri? = data.data
                Log.i("MainActivity","onActivityResult Uri to INPUT:$uri")
                if (flagUri == 0){
                    equalsUri(0,uri!!)
                    firstSound = uri
                    btnFirstMp3!!.text = "Файл добавлен"
                }else{
                    equalsUri(1,uri!!)
                    secondSound = uri
                    btnSecondMp3!!.text = "Файл добавлен"
                }
            }
        }
    }

    // Проверка на изменение проигрываемого листа
    private fun equalsUri(i: Int,uri: Uri) {
        if (sounds.size > 0){
            if (sounds[i] != uri){
                loopingPlay!!.isDataChanged = true
                Log.i("MinActivity","DATA CHANGED")
            }
        }
    }
}
