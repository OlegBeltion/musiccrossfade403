package com.example.crossfademusic403

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.concurrent.timerTask

class Player {


    var mediaPlayer : MediaPlayer? =  null
    private var song : Uri? = null
    private var isPaused = false // Флаг определяет была ли поставлена пауза
    private var pauseSongTime : Int = 0 // Время песни, поставленной на паузу
    private var ctx: Context? = null
    var volume: Float = 0.0f



    fun initData(song: Uri, ctx: Context){
        Log.i("Player", "---Player---")
        this.song = song
        this.ctx = ctx
        preparePlayer()
        setPlayerListener()
    }

    fun play(){
        try {
            Log.i("Player", "play()")
            mediaPlayer!!.start()
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
    private fun preparePlayer(){
        try {
            if (mediaPlayer == null){
                Log.i("Player", "!!!")
                mediaPlayer = MediaPlayer.create(ctx!!,song)/* Работает корректно, но в логах ошибка
                Искал в интернете, писал на форум - решения не нашёл
                E/ExtMediaPlayer-JNI: env->IsInstanceOf fails
                E/MediaPlayer-JNI: JNIMediaPlayerFactory: bIsQCMediaPlayerPresent 0
                E/ExtMediaPlayer-JNI: env->IsInstanceOf fails
                E/MediaPlayer-JNI: JNIMediaPlayerFactory: bIsQCMediaPlayerPresent 0
                 */
                Log.i("Player", "!!!")
                Log.i("Player", "isPaused:$isPaused")
                Log.i("Player", "pauseSongTime:$pauseSongTime")
                if (isPaused){
                    mediaPlayer!!.seekTo(pauseSongTime)
                }
            }else {
                mediaPlayer!!.reset()
                mediaPlayer!!.setDataSource(ctx!!, song!!)
                mediaPlayer!!.prepare()
            }
        }   catch (e: IllegalStateException) {
            Log.d("Player", "IllegalStateException: " + e.message);
        }
        catch (e: IOException) {
            Log.d("Player", "IOException: " + e.message);
        }
        catch (e: IllegalArgumentException) {
            Log.d("Player", "IllegalArgumentException: " + e.message);
        }
        catch (e: SecurityException) {
            Log.d("Player", "SecurityException: " + e.message);
        }
    }

    private fun setPlayerListener() {
        mediaPlayer!!.setOnErrorListener{ player, what, extra -> // игнорирует ошибку, описанную выше
            Log.i("Player", "OnErorrListener: $player -> error:$what,code error:$extra")
            true // True, если метод обработал ошибку, и false, если нет. Возврат false или отсутствие OnErrorListener вообще вызовет OnCompletionListener.
        }
        mediaPlayer!!.setOnCompletionListener{
            Log.i("Player", "Completion play")
        }

    }

    fun setVolumeByZero(){
        mediaPlayer!!.setVolume(0f,0f)
        Log.i("Player", "setVolumeByZero() volume:0")
    }

    fun setDownVolume(defaultVolume: Float) {
        volume -= defaultVolume
        mediaPlayer!!.setVolume(volume,volume)
        Log.i("Player","setDownVolume:$volume/$defaultVolume")
    }

    fun setUpVolume(defaultVolume: Float) {
        volume += defaultVolume
        mediaPlayer!!.setVolume(volume,volume)
        Log.i("Player","setUpVolume:$volume/$defaultVolume")

    }



//    private fun changeCurrentPlayer() {
//        try {
//            if (mediaPlayer == null){
//                Log.i("Player", "!!!")
//                mediaPlayer = MediaPlayer.create(ctx!!,song)/* Работает корректно, но в логах ошибка
//
//                E/ExtMediaPlayer-JNI: env->IsInstanceOf fails
//                E/MediaPlayer-JNI: JNIMediaPlayerFactory: bIsQCMediaPlayerPresent 0
//                E/ExtMediaPlayer-JNI: env->IsInstanceOf fails
//                E/MediaPlayer-JNI: JNIMediaPlayerFactory: bIsQCMediaPlayerPresent 0
//                 */
//                Log.i("Player", "!!!")
//                Log.i("Player", "isPaused:$isPaused")
//                Log.i("Player", "pauseSongTime:$pauseSongTime")
//                if (isPaused){
//                    mediaPlayer!!.seekTo(pauseSongTime)
//                }
//            }else {
//                mediaPlayer!!.reset()
//                mediaPlayer!!.setDataSource(ctx!!, song!!)
//                mediaPlayer!!.prepare()
//            }
//        }   catch (e: IllegalStateException) {
//            Log.d("Player", "IllegalStateException: " + e.message);
//        }
//        catch (e: IOException) {
//            Log.d("Player", "IOException: " + e.message);
//        }
//        catch (e: IllegalArgumentException) {
//            Log.d("Player", "IllegalArgumentException: " + e.message);
//        }
//        catch (e: SecurityException) {
//            Log.d("Player", "SecurityException: " + e.message);
//        }
//    }

    fun pause(){
        if (mediaPlayer!!.isPlaying){
            Log.i("Player", "Pause")
            mediaPlayer!!.pause()
            pauseSongTime = mediaPlayer!!.currentPosition
            Log.i("Player", "Pause time:$pauseSongTime")
            mediaPlayer!!.reset()
            mediaPlayer!!.release()
            mediaPlayer = null
            isPaused = true
        }
    }

}



