package com.example.crossfademusic403

import android.content.Context
import android.net.Uri
import android.util.Log
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class LoopingPlay {
    private var songs: ArrayList<Uri>? = null
    private var ctx: Context? = null
    var duration: Long = 2
    private var players: ArrayList<Player> = ArrayList()
    private var currentIndexPlayer = 0
    var isPaused = false // Флаг определяет была ли поставлена пауза
    var isDataChanged : Boolean = false // Флаг для проверки вторичного и последующих изменений содержимого URI
    var logTag = "LoopingPlay"

    init {
    }

    fun initData(sounds: ArrayList<Uri>, applicationContext: Context, duration: Int) {
        Log.i(logTag, "---LoopingPlay---")
        this.songs = sounds
        this.ctx = applicationContext
        this.duration = duration * 1000L
        initPlayer()
    }

    private fun initPlayer() {
        players!!.clear()
        for (elem in songs!!){
            var player = Player()
            player.initData(elem,ctx!!)
            players.add(player)
        }
    }

    private fun changeCurrentPlayer(){
        currentIndexPlayer = if(currentIndexPlayer == 0){
            1
        } else {
            0
        }
    }

    // Циклическое проигрывание
    fun play(){
        Log.i(logTag, "play()")
        players[0].mediaPlayer!!.setOnPreparedListener {
            fadeMusic(players[0])
            riseMusic(players[0])
        }
        players[0].play()
        players[0].mediaPlayer!!.setOnCompletionListener {
            Log.i(logTag, "currentIndexPlayer Completion")
        }

    }

    private fun fadeMusic(player: Player) {
        val timeStart: Long = player.mediaPlayer?.duration!!.toLong() - duration // время начала кройфейда
        Log.i(logTag, "startFadeMusic player[$currentIndexPlayer] timeStartFade:$timeStart")
//        Log.i(logTag, "startFadeMusic mediaPlayer!!.duration:${player.mediaPlayer!!.duration}")
        val period : Long = 250
        val steps: Int = (duration / period).toInt()
//        Log.i(logTag, "startFadeMusic Steps:$steps")
        val volumeStep: Float = 1 / steps.toFloat()
//        Log.i(logTag, "startCrossFadeMusic Volume stap:$volumeStep")
        val timer = Timer(true)
        val timerTask: TimerTask = timerTask {
            run {
                Log.i(logTag, "player[$currentIndexPlayer] At position::${player.mediaPlayer!!.currentPosition}")
                if (player.volume <= 0f){
                    player.volume = 0f
                    Log.i(logTag, "startFadeMusic cancel()/purge timer")
                    timer.cancel()
                    timer.purge()
                } else {
                    Log.i(logTag, "startFadeMusic mediaPlayer.volume change")
                    player.setDownVolume(volumeStep)
                }

            }
        }

        val timerTaskNext: TimerTask = timerTask {
            run {
                loopingPlay()
            }
        }
        Timer(true).schedule(timerTaskNext,timeStart)
        timer.schedule(timerTask,timeStart,period)
    }

    private fun loopingPlay() {
        changeCurrentPlayer()
        Log.i(logTag, "---loopingPlay()---")
        Log.i(logTag, "currentIndexPlayer:$currentIndexPlayer")
        riseMusic(players[currentIndexPlayer])
        fadeMusic(players[currentIndexPlayer])
        players[currentIndexPlayer].play()
        players[currentIndexPlayer].mediaPlayer!!.setOnCompletionListener {
            Log.i(logTag, "currentIndexPlayer Completion")
        }

    }

    private fun riseMusic(player: Player) {
        player.setVolumeByZero()
//        Log.i(logTag, "startRiseMusic mediaPlayer!!.duration:${player.mediaPlayer!!.duration}")
        val period : Long = 250
        val steps: Int = (duration / period).toInt()
//        Log.i(logTag, "startRiseMusic Steps:$steps")
        val volumeStep: Float = 1 / steps.toFloat()
//        Log.i(logTag, "startRiseMusic Volume stap:$volumeStep")
        val timer = Timer(true)
        var timerTask: TimerTask = timerTask {
            run {
                Log.i(logTag, "player[$currentIndexPlayer] At position:${player.mediaPlayer!!.currentPosition}")
                if (player.volume >= 1f){
                    player.volume = 1f
                    Log.i(logTag, "startRiseMusic cancel()/purge timer")
                    timer.cancel()
                    timer.purge()
                } else {
                    Log.i(logTag, "startRiseMusic mediaPlayer.volume change")
                    player.setUpVolume(volumeStep)
                }

            }
        }
        timer.schedule(timerTask,0,period)
    }



    fun resume(){
    }

}



